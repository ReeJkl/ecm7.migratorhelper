﻿using ECM7.Migrator.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Sdk.ECM7MigratorHelper
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MigrationColumnAttribute : MigrationSettingsAttribute
    {
        public string ColumnName { get; set; }

        public static DbType DefaultColumnType = DbType.Object;
        private DbType _columnType = DefaultColumnType;
        public DbType ColumnType
        {
            get { return _columnType; }
            set { _columnType = value; }
        }

        public static int DefaultColumnTypeSize = -1;
        private int _columnTypeSize = DefaultColumnTypeSize;
        public int ColumnTypeSize
        {
            get { return _columnTypeSize; }
            set { _columnTypeSize = value; }
        }

        public bool IsPrimaryKey { get; set; }
        public bool IsOneOfManyPrimaryKeys { get; set; }
        public bool IsUnique { get; set; }
        public bool IsNull { get; set; }
        public bool IsNotNull { get; set; }
        public bool IsIdentity { get; set; }

        public object DefaultValue { get; set; }

        public string RefTableName { get; set; }
        public Type RefTableType { get; set; }
        public string RefColumnName { get; set; }

        private ColumnProperty? _columnProperty = null;
        public ColumnProperty ColumnProperty
        {
            get
            {
                if (_columnProperty.HasValue)
                {
                    return _columnProperty.Value;
                }
                else
                {
                    var result = ECM7.Migrator.Framework.ColumnProperty.None;

                    if (IsPrimaryKey)
                    {
                        result = result | ECM7.Migrator.Framework.ColumnProperty.PrimaryKey;
                    }
                    if (IsIdentity)
                    {
                        result = result | ECM7.Migrator.Framework.ColumnProperty.Identity;
                    }
                    if (IsUnique)
                    {
                        result = result | ECM7.Migrator.Framework.ColumnProperty.Unique;
                    }
                    if (IsNull)
                    {
                        result = result | ECM7.Migrator.Framework.ColumnProperty.Null;
                    }
                    if (IsNotNull)
                    {
                        result = result | ECM7.Migrator.Framework.ColumnProperty.NotNull;
                    }
                    return result;
                }
            }
            set
            {
                _columnProperty = value;
            }
        }
    }
}
