﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Sdk.ECM7MigratorHelper
{
    [ConfigurationCollection(typeof(MigrationElement))]
    public class MigrationsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MigrationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MigrationElement)(element)).Assembly;
        }
    }
}
