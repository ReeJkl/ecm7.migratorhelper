﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;

namespace Sdk.ECM7MigratorHelper
{
    public static class MigrationConfigurator
    {
        public static void Migrate()
        {
            var migrationsSection = (MigratorSection)ConfigurationManager.GetSection("migrator");
            if (migrationsSection == null)
            {
                throw new ArgumentNullException("migrations", "Секция migrations не определена в конфигурации.");
            }
            foreach (MigrationElement migration in migrationsSection.Migrations)
            {
                if (migrationsSection == null)
                {
                    throw new ArgumentNullException("migration", "Секция migration не определена в секции migrations.");
                }

                if (string.IsNullOrWhiteSpace(migration.ConnectionString) && string.IsNullOrWhiteSpace(migration.ConnectionStringName))
                {
                    throw new ArgumentException("Один из параметров ConnectionString или ConnectionStringName должен быть задан.");
                }

                var assembly = Assembly.Load(migration.Assembly);
                if (!string.IsNullOrWhiteSpace(migration.ConnectionStringName))
                {
                    Migrate(migration.ProviderName, migration.ConnectionStringName, assembly, migration.Version);
                }
                else
                {
                    Migrate(migration.ProviderName, migration.ConnectionString, assembly, migration.Version);
                }
            }
        }

        public static void Migrate(long version)
        {
            var migrationsSection = (MigratorSection)ConfigurationManager.GetSection("migrator");
            if (migrationsSection == null)
            {
                throw new ArgumentNullException("migrations", "Секция migrations не определена в конфигурации.");
            }
            foreach (MigrationElement migration in migrationsSection.Migrations)
            {
                if (migrationsSection == null)
                {
                    throw new ArgumentNullException("migration", "Секция migration не определена в секции migrations.");
                }

                if (string.IsNullOrWhiteSpace(migration.ConnectionString) && string.IsNullOrWhiteSpace(migration.ConnectionStringName))
                {
                    throw new ArgumentException("Один из параметров ConnectionString или ConnectionStringName должен быть задан.");
                }

                var assembly = Assembly.Load(migration.Assembly);
                if (!string.IsNullOrWhiteSpace(migration.ConnectionStringName))
                {
                    Migrate(migration.ProviderName, migration.ConnectionStringName, assembly, version);
                }
                else
                {
                    Migrate(migration.ProviderName, migration.ConnectionString, assembly, version);
                }
            }
        }

        public static void Migrate(string providerName, string connectionNameOrConnectionString, Assembly assembly, long version)
        {
            string connectionString = connectionNameOrConnectionString;

            var connectionStringsSection = (ConnectionStringsSection)ConfigurationManager.GetSection("connectionStrings");
            if (connectionStringsSection != null)
            {
                var connectionStringName = connectionStringsSection.ConnectionStrings.Cast<ConnectionStringSettings>().FirstOrDefault(item => item.Name == connectionNameOrConnectionString);
                if (connectionStringName != null)
                {
                    connectionString = connectionStringName.ConnectionString;
                }
            }

            var migrator = new ECM7.Migrator.Migrator(providerName, connectionString, assembly);
            migrator.Migrate(version);
        }
    }
}
