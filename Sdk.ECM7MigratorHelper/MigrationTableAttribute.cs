﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdk.ECM7MigratorHelper
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MigrationTableAttribute : MigrationSettingsAttribute
    {
        public string TableName { get; set; }
        public string SchemaName { get; set; }
    }
}