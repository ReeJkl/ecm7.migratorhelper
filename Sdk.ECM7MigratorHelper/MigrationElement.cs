﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdk.ECM7MigratorHelper
{
    public class MigrationElement : ConfigurationElement
    {
        [ConfigurationProperty("ProviderName", DefaultValue = "ECM7.Migrator.Providers.PostgreSQL.PostgreSQLTransformationProvider, ECM7.Migrator.Providers.PostgreSQL", IsRequired = true)]
        public string ProviderName
        {
            get
            {
                return (string)this["ProviderName"];
            }
            set
            {
                this["ProviderName"] = value;
            }
        }

        [ConfigurationProperty("ConnectionString", IsRequired = false)]
        public string ConnectionString
        {
            get
            {
                return (string)this["ConnectionString"];
            }
            set
            {
                this["ConnectionString"] = value;
            }
        }

        [ConfigurationProperty("ConnectionStringName", IsRequired = false)]
        public string ConnectionStringName
        {
            get
            {
                return (string)this["ConnectionStringName"];
            }
            set
            {
                this["ConnectionStringName"] = value;
            }
        }

        [ConfigurationProperty("Assembly", IsRequired = true)]
        public string Assembly
        {
            get
            {
                return (string)this["Assembly"];
            }
            set
            {
                this["Assembly"] = value;
            }
        }

        [ConfigurationProperty("Version", DefaultValue = -1L, IsRequired = true)]
        public long Version
        {
            get
            {
                return (long)this["Version"];
            }
            set
            {
                this["Version"] = value;
            }
        }
    }
}
