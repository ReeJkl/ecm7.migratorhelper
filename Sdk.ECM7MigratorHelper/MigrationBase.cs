﻿using ECM7.Migrator.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sdk.ECM7MigratorHelper
{
    public abstract class MigrationBase : Migration
    {
        protected string _defaultSchemaName = "public";

        protected void AddInt32IdColumn(string tableName, string columnName = "id")
        {
            Database.AddColumn(tableName,
                new Column(columnName, new ColumnType(System.Data.DbType.Int32), ColumnProperty.PrimaryKey | ColumnProperty.Unique | ColumnProperty.NotNull));
        }
        protected void AddGuidIdColumn(string tableName, string columnName = "id")
        {
            AddGuidColumn(tableName, columnName);
            Database.AddPrimaryKey(tableName + "_pkey", tableName, columnName);
        }
        protected void AddGuidColumn(string tableName, string columnName)
        {
            Database.ExecuteNonQuery(Database.FormatSql("ALTER TABLE {0:NAME} ADD COLUMN {1:NAME} UUID;", tableName, columnName));
        }
        protected void AddUniqueNameColumn(string tableName, string columnName = "name")
        {
            Database.AddColumn(tableName,
                new Column(columnName, new ColumnType(System.Data.DbType.String), ColumnProperty.Unique | ColumnProperty.NotNull));
        }
        protected void AddRelation(string tableName, string columnName, string refTableName, string refColumnName,
            ECM7.Migrator.Framework.ForeignKeyConstraint updateRule = ECM7.Migrator.Framework.ForeignKeyConstraint.Cascade,
            ECM7.Migrator.Framework.ForeignKeyConstraint deleteRule = ECM7.Migrator.Framework.ForeignKeyConstraint.Cascade)
        {
            var keyName = GetRelationName(tableName, columnName, refTableName, refColumnName);
            Database.AddForeignKey(keyName,
                new SchemaQualifiedObjectName() { Name = tableName, Schema = "public" },
                columnName,
                new SchemaQualifiedObjectName() { Name = refTableName, Schema = "public" },
                refColumnName,
                deleteRule,
                updateRule
            );

        }
        protected string GetRelationName(string tableName, string columnName, string refTableName, string refColumnName)
        {
            return tableName + "_" + columnName + "_fkey";
        }

        protected void AddTableWithGuidKeyAndName(string tableName, string idColumnName = "id", string nameColumnName = "name")
        {
            Database.AddTable(tableName);
            AddGuidIdColumn(tableName, idColumnName);
            Database.AddColumn(tableName, new Column(nameColumnName, new ColumnType(System.Data.DbType.String), ColumnProperty.NotNull));
        }
        protected void AddTableWithInt32KeyAndName(string tableName, string idColumnName = "id", string nameColumnName = "name")
        {
            Database.AddTable(tableName,
                new Column(idColumnName, new ColumnType(System.Data.DbType.Int32), ColumnProperty.PrimaryKey | ColumnProperty.NotNull | ColumnProperty.Unique),
                new Column(nameColumnName, new ColumnType(System.Data.DbType.String), ColumnProperty.NotNull));

        }

        protected void AddGuidGuidRelationTable(string tableName, string parentColumnName, string childrenColumnName,
            string parentTableName, string childrenTableName,
            string parentTableColumnName, string childrenTableColumnName)
        {
            Database.AddTable(tableName);
            AddGuidColumn(tableName, parentColumnName);
            AddGuidColumn(tableName, childrenColumnName);

            Database.AddPrimaryKey(tableName + "_pkey", tableName, parentColumnName, childrenColumnName);

            // relations
            Database.AddForeignKey(GetRelationName(tableName, parentColumnName, parentTableName, parentTableColumnName), tableName, parentColumnName, parentTableName, parentTableColumnName);
            Database.AddForeignKey(GetRelationName(tableName, childrenColumnName, childrenTableName, childrenTableColumnName), tableName, childrenColumnName, childrenTableName, childrenTableColumnName);
        }
        protected void AddGuidGuidRelationTable(string tableName, string parentColumnName, string childrenColumnName,
            string parentTableName, string childrenTableName)
        {
            AddGuidGuidRelationTable(tableName, parentColumnName, childrenColumnName, parentTableName, childrenTableName, parentColumnName, childrenColumnName);
        }

        protected void AddGuidInt32RelationTable(string tableName, string parentColumnName, string childrenColumnName,
            string parentTableName, string childrenTableName,
            string parentTableColumnName, string childrenTableColumnName)
        {
            Database.AddTable(tableName);
            AddGuidColumn(tableName, parentColumnName);
            Database.AddColumn(tableName, new Column(childrenColumnName, new ColumnType(System.Data.DbType.Int32), ColumnProperty.NotNull));

            Database.AddPrimaryKey(tableName + "_pkey", tableName, parentColumnName, childrenColumnName);

            // relations
            Database.AddForeignKey(GetRelationName(tableName, parentColumnName, parentTableName, parentTableColumnName), tableName, parentColumnName, parentTableName, parentTableColumnName);
            Database.AddForeignKey(GetRelationName(tableName, childrenColumnName, childrenTableName, childrenTableColumnName), tableName, childrenColumnName, childrenTableName, childrenTableColumnName);
        }
        protected void AddGuidInt32RelationTable(string tableName, string parentColumnName, string childrenColumnName,
            string parentTableName, string childrenTableName)
        {
            AddGuidInt32RelationTable(tableName, parentColumnName, childrenColumnName, parentTableName, childrenTableName, parentColumnName, childrenColumnName);
        }

        protected void ApplyFromTypes(params Type[] types)
        {
            var currentMigrationType = this.GetType();

            // создание таблиц
            var tables = (from type in types
                          let attribute = type.GetCustomAttributes(typeof(MigrationTableAttribute), true).Cast<MigrationTableAttribute>().SingleOrDefault()
                          let name = attribute == null ? type.Name : attribute.TableName ?? GetTableNameFromTypeName(type.Name)
                          let schema = attribute == null ? _defaultSchemaName : attribute.SchemaName ?? _defaultSchemaName
                          select new { TableType = type, TableSettings = attribute, Name = name, Schema = schema }).ToList();

            var creatingTables = from table in tables
                                 where table.TableSettings == null || table.TableSettings.MigrationType.Equals(currentMigrationType)
                                 let order = table.TableSettings == null ? 0 : table.TableSettings.Order
                                 select new { Name = table.Name, Schema = table.Schema, Order = order };

            foreach (var table in creatingTables.OrderBy(item => item.Order))
            {
                Database.AddTable(new SchemaQualifiedObjectName() { Name = table.Name, Schema = table.Schema });
            }

            // создание колонок
            var columns = from table in tables
                          from property in table.TableType.GetProperties()
                          let attribute = property.GetCustomAttributes(typeof(MigrationColumnAttribute), true).Cast<MigrationColumnAttribute>().SingleOrDefault()
                          where attribute != null
                          let columnName = attribute.ColumnName ?? property.Name
                          select new { TableName = table.Name, TableSchema = table.Schema, ColumnInfo = property, ColumnSettings = attribute, ColumnName = columnName };

            var creatingColumns = from column in columns
                                  where column.ColumnSettings.MigrationType.Equals(currentMigrationType)
                                  let migrationColumnType = column.ColumnSettings.ColumnType == MigrationColumnAttribute.DefaultColumnType ? GetDbType(column.ColumnInfo.PropertyType) :
                                                                                                       GetDbType(column.ColumnSettings.ColumnType, column.ColumnSettings.ColumnTypeSize)
                                  select new { ColumnItem = column, Order = column.ColumnSettings.Order, MigrationColumnType = migrationColumnType };
            foreach (var column in creatingColumns.OrderBy(item => item.Order))
            {
                if (column.MigrationColumnType.DataType == DbType.Guid)
                {
                    if (column.ColumnItem.ColumnSettings.IsPrimaryKey)
                    {
                        AddGuidIdColumn(column.ColumnItem.TableName, column.ColumnItem.ColumnName);
                    }
                    else
                    {
                        AddGuidColumn(column.ColumnItem.TableName, column.ColumnItem.ColumnName);
                    }
                }
                else
                {
                    Database.AddColumn(new SchemaQualifiedObjectName() { Name = column.ColumnItem.TableName, Schema = column.ColumnItem.TableSchema },
                        new Column(column.ColumnItem.ColumnName, column.MigrationColumnType, column.ColumnItem.ColumnSettings.ColumnProperty, column.ColumnItem.ColumnSettings.DefaultValue));
                }
            }
            // создание многоколоночных ключей
            var manyColumnKeys = from column in creatingColumns
                                 where column.ColumnItem.ColumnSettings.IsOneOfManyPrimaryKeys
                                 orderby column.Order
                                 group column by new { TableName = column.ColumnItem.TableName, Schema = column.ColumnItem.TableSchema };
            foreach (var columnGroup in manyColumnKeys)
            {
                Database.AddPrimaryKey("PK_" + columnGroup.Key.TableName,
                    new SchemaQualifiedObjectName() { Name = columnGroup.Key.TableName, Schema = columnGroup.Key.Schema },
                    columnGroup.Select(item => item.ColumnItem.ColumnName).ToArray());
            }
            // создание связей
            var refColumns = from column in creatingColumns
                             let refTableName = column.ColumnItem.ColumnSettings.RefTableName
                             let refTableType = column.ColumnItem.ColumnSettings.RefTableType
                             where refTableType != null || refTableName != null
                             let refColumnName = column.ColumnItem.ColumnSettings.RefColumnName ?? column.ColumnItem.ColumnName
                             select new
                             {
                                 TableName = column.ColumnItem.TableName,
                                 ColumnName = column.ColumnItem.ColumnName,
                                 RefTableName = refTableName,
                                 RefTableType = refTableType,
                                 RefColumnName = refColumnName,
                                 Order = column.Order
                             };
            foreach (var column in refColumns)
            {
                string refTableName;
                if (column.RefTableType != null)
                {
                    var findedTable = tables.FirstOrDefault(item => item.TableType.Equals(column.RefTableType));
                    if (findedTable != null)
                    {
                        refTableName = findedTable.Name;
                    }
                    else
                    {
                        refTableName = column.RefTableType.Name;
                    }
                }
                else
                {
                    refTableName = column.RefTableName;
                }
                AddRelation(column.TableName, column.ColumnName, refTableName, column.RefColumnName);
            }
        }

        private string GetTableNameFromTypeName(string name)
        {
            var splitSimbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder result = new StringBuilder();
            result.Append(name.First());
            foreach (var simbol in name.Skip(1))
            {
                if (splitSimbols.Contains(simbol))
                {
                    result.Append("s");
                }
                result.Append(simbol);
            }
            result.Append("s");
            return result.ToString();
        }
        private ColumnType GetDbType(Type type)
        {
            if (type.Equals(typeof(short)))
            {
                return new ColumnType(DbType.Int16);
            }
            if (type.Equals(typeof(short?)))
            {
                return new ColumnType(DbType.Int16);
            }
            if (type.Equals(typeof(int)))
            {
                return new ColumnType(DbType.Int32);
            }
            if (type.Equals(typeof(int?)))
            {
                return new ColumnType(DbType.Int32);
            }
            if (type.Equals(typeof(long)))
            {
                return new ColumnType(DbType.Int64);
            }
            if (type.Equals(typeof(DateTime)))
            {
                return new ColumnType(DbType.DateTime);
            }
            if (type.Equals(typeof(DateTime?)))
            {
                return new ColumnType(DbType.DateTime);
            }
            if (type.Equals(typeof(double)))
            {
                return new ColumnType(DbType.Double);
            }
            if (type.Equals(typeof(double?)))
            {
                return new ColumnType(DbType.Double);
            }
            if (type.Equals(typeof(string)))
            {
                return new ColumnType(DbType.String);
            }
            if (type.Equals(typeof(float)))
            {
                return new ColumnType(DbType.Single);
            }
            if (type.Equals(typeof(Guid)))
            {
                return new ColumnType(DbType.Guid);
            }
            if (type.Equals(typeof(Guid?)))
            {
                return new ColumnType(DbType.Guid);
            }
            if (type.Equals(typeof(byte[])))
            {
                return new ColumnType(DbType.Binary);
            }
            if (type.Equals(typeof(bool)))
            {
                return new ColumnType(DbType.Boolean);
            }
            if (type.Equals(typeof(byte)))
            {
                return new ColumnType(DbType.Byte);
            }
            if (type.Equals(typeof(TimeSpan)))
            {
                return new ColumnType(DbType.Time);
            }
            if (type.Equals(typeof(TimeSpan?)))
            {
                return new ColumnType(DbType.Time);
            }
            return null;
        }
        private ColumnType GetDbType(DbType dbType, int size)
        {
            if (size != MigrationColumnAttribute.DefaultColumnTypeSize)
            {
                return new ColumnType(dbType, size);
            }
            else
            {
                return new ColumnType(dbType);
            }
        }

        protected void RevertFromTypes(params Type[] types)
        {
            var currentMigrationType = this.GetType();

            var tables = (from type in types
                          let attribute = type.GetCustomAttributes(typeof(MigrationTableAttribute), true).Cast<MigrationTableAttribute>().SingleOrDefault()
                          let name = attribute == null ? type.Name : attribute.TableName ?? GetTableNameFromTypeName(type.Name)
                          let schema = attribute == null ? _defaultSchemaName : attribute.SchemaName ?? _defaultSchemaName
                          select new { TableType = type, TableSettings = attribute, Name = name, Schema = schema }).ToList();

            // удаление таблиц
            var deletingTables = from table in tables
                                 where table.TableSettings == null || table.TableSettings.MigrationType.Equals(currentMigrationType)
                                 let order = table.TableSettings == null ? 0 : table.TableSettings.Order
                                 select new { Name = table.Name, Schema = table.Schema, Order = order };
            foreach (var table in deletingTables.OrderByDescending(item => item.Order))
            {
                Database.RemoveTable(new SchemaQualifiedObjectName() { Name = table.Name, Schema = table.Schema });
            }
            // удаление колонок
            var columnsFromNotDeletedTables = from table in tables
                                              where !(table.TableSettings == null || table.TableSettings.MigrationType.Equals(currentMigrationType))
                                              from property in table.TableType.GetProperties()
                                              let attribute = property.GetCustomAttributes(typeof(MigrationColumnAttribute), true).Cast<MigrationColumnAttribute>().SingleOrDefault()
                                              where attribute != null
                                              let columnName = attribute.ColumnName ?? property.Name
                                              select new { TableName = table.Name, TableSchema = table.Schema, ColumnSettings = attribute, ColumnName = columnName };

            var deletingColumns = from column in columnsFromNotDeletedTables
                                  where column.ColumnSettings.MigrationType.Equals(currentMigrationType)
                                  select new { ColumnItem = column, Order = column.ColumnSettings.Order };

            foreach (var column in deletingColumns.OrderByDescending(item => item.Order))
            {
                Database.RemoveColumn(new SchemaQualifiedObjectName() { Name = column.ColumnItem.TableName, Schema = column.ColumnItem.TableSchema }, column.ColumnItem.ColumnName);
            }
        }

        protected void ApplyFromAssembly(string assemblyName)
        {
            ApplyFromAssembly(Assembly.Load(assemblyName));
        }
        protected void RevertFromAssembly(string assemblyName)
        {
            RevertFromAssembly(Assembly.Load(assemblyName));
        }
        protected void ApplyFromAssembly(Assembly assembly)
        {
            var migrationTypes = from type in assembly.GetTypes()
                                 where type.GetCustomAttributes(typeof(MigrationSettingsAttribute), true).FirstOrDefault() != null
                                 select type;
            ApplyFromTypes(migrationTypes.ToArray());
        }
        protected void RevertFromAssembly(Assembly assembly)
        {
            var migrationTypes = from type in assembly.GetTypes()
                                 where type.GetCustomAttributes(typeof(MigrationSettingsAttribute), true).FirstOrDefault() != null
                                 select type;
            RevertFromTypes(migrationTypes.ToArray());
        }
    }
}

