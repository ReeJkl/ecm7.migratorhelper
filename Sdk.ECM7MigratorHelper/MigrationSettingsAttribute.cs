﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdk.ECM7MigratorHelper
{
    public class MigrationSettingsAttribute : Attribute
    {
        /// <summary>
        /// Тип миграции (класс, в котором она описана)
        /// </summary>
        public Type MigrationType { get; set; }
        /// <summary>
        /// Порядок добавления
        /// </summary>
        public int Order { get; set; }
    }
}