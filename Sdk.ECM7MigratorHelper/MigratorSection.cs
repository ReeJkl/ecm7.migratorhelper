﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdk.ECM7MigratorHelper
{
    public class MigratorSection : ConfigurationSection
    {
        [ConfigurationProperty("migrations", IsRequired = true)]
        public MigrationsCollection Migrations
        {
            get
            {
                return (MigrationsCollection)this["migrations"];
            }
            set
            {
                this["migrations"] = value;
            }
        }
    }
}
